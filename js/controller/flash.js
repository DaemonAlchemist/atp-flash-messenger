/**
 * Created by Andy on 9/8/2016.
 */

import load from "../../../core/js/loader.js";

export default () => {
    $("body").append('<div id="flash-messages" class="col-xs-12 row"></div>');
    let container = $("div#flash-messages");

    $(document).on('atp.messages.new', (e, messages) => {
        messages.map(message => {
            if(message.type == 'error') {
                message.type = 'danger';
            }
            message.Type = message.type.charAt(0).toUpperCase() + message.type.slice(1);
            load.template("module/flash/views/flash-alert")
                .then(template => container.append(template(message)));
        });

        setTimeout(() => container.html(""), 5000);
    });
}
