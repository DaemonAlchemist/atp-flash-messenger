/**
 * Created by Andy on 9/8/2016.
 */

import flashController from "./controller/flash.js";

export default () => {
    flashController();
}